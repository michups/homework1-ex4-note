package pl.codementors.notes.model;

/**
 * Represents some notepad which can store up to 20 notes.
 */
public class Notepad {

    /*
    The value 20 is nasty hardcoded. This will be changed in future examples to something more fancy.
     */

    /*
    Assigning value to the field in the same place as declaration causes that this value will be newly created for
    every newly created object of Notepad class.
     */

    /*
    Arrays are usually named in plural form describing what they are storing.
     */

    /**
     * Notes stored by the notepad.
     */
    private Note[] notes = new Note[20];

    /*
    In this case, we don't want to give access to the whole notes field, so we are allowing access to specified notes.
     */

    /**
     *
     * @param index Index of the note to be returned.
     * @return Note from the specified index, or null if there is no such note.
     */
    public Note getNote(int index) {
        if (index >= 0 && index < notes.length) {
            // Index should be in range [0, notes.length) or [0, notes.length-1].
            return notes[index];
        } else {
            return null;
        }
    }

    /**
     *
     * @param index Index where new note should be stored.
     * @param note New note which will be stored at index.
     */
    public void setNote(int index, Note note) {
        if (index >= 0 && index < notes.length) {
            notes[index] = note;
        }
    }

    /**
     * Prints all the notes to standard output.
     * The way each note is displayed on the console ([] is replaced with proper values):
     * --BEGIN NOTE--
     * INDEX: [note index]
     * TITLE: [note title]
     * CONTENT: [content]
     * --END NOTE--
     * or if there is no note at the index:
     * --BEGIN NOTE--
     * INDEX: [note index]
     * --EMPTY--
     * --END NOTE--
     */
    public void printNotesToOutput() {
        for (int i=0; i<notes.length; i++){
            System.out.println("--BEGIN NOTE--");
            System.out.println("INDEX: "+i);
            if(notes[i]==null)
                System.out.println("--EMPTY--");
            else
            {
                System.out.println("TITLE: " + notes[i].getTitle());
                System.out.println("CONTENT: " + notes[i].getContent());
            }
            System.out.println("--END NOTE--");
        }

    }

}
